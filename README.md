# Raiting Component

This is my first vue component 😊 . It's been created following Sarah Dayan [tutorial](https://frontstuff.io/build-your-first-vue-js-component)

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
